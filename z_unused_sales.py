# coding=utf-8
"""
ads factRepairOrder
pg  ads.ext_fact_repair_order_tmp
pg  ads.ext_fact_repair_order
Not 100% confident - do a nightly check of the final product in pg vs ads

"""
import db_cnx
import ops
import string
import csv

task = 'sales'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_fact_repair_order.csv'
try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        'Failed dependency check'
        exit()
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.*
                from factrepairorder a
                inner join day b on a.opendatekey = b.datekey
                where b.thedate between curdate() - 45 AND curdate()
                UNION -- all
                SELECT a.*
                from factrepairorder a
                inner join day b on a.closedatekey = b.datekey
                where b.thedate between curdate() - 45 AND curdate()
                UNION -- all
                SELECT a.*
                from factrepairorder a
                inner join day b on a.finalclosedatekey = b.datekey
                where b.thedate between curdate() - 45 AND curdate()
                UNION -- all
                SELECT a.*
                from factrepairorder a
                inner join day b on a.linedatekey = b.datekey
                where b.thedate between curdate() - 45 AND curdate()
                UNION -- all
                SELECT a.*
                from factrepairorder a
                inner join day b on a.flagdatekey = b.datekey
                where b.thedate between curdate() - 45 AND curdate()
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_fact_repair_order_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_fact_repair_order_tmp from stdin with csv encoding 'latin-1 '""", io)
            # ads.ext_fact_repair_order
            sql = """
                delete
                from ads.ext_fact_repair_order
                where ro in (
                  select ro
                  from ads.ext_fact_repair_order_tmp);
            """
            pg_cur.execute(sql)
            # ads.ext_fact_repair_order
            sql = """
                insert into ads.ext_fact_repair_order
                select * from ads.ext_fact_repair_order_tmp
            """
            pg_cur.execute(sql)
            # bspp.sales_tmp
            pg_cur.execute("truncate bspp.sales_tmp")
            sql = """
                insert into bspp.sales_tmp
                select n.writernumber, m.the_date, m.control, m.parts, m.labor
                from ( -- m
                  select (
                    select distinct servicewriterkey
                    from ads.ext_fact_repair_order
                    where ro = a.control),
                    e.the_date, a.control,
                    sum(case when c.category = 'parts' then -1 * a.amount else 0 end) as parts,
                    sum(case when c.category = 'labor' then -1 * a.amount else 0 end) as labor
                  from fin.fact_gl a
                  inner join fin.dim_account b on a.account_key = b.account_key
                  inner join bspp.sale_accounts c on b.account = c.account
                  inner join dds.dim_date e on a.date_key = e.date_key
                    and e.biweekly_pay_period_sequence = (
                      select biweekly_pay_period_sequence
                      from dds.dim_date
                      where the_date = current_date)
                  where a.post_status = 'Y'
                  group by servicewriterkey, e.the_date, a.control) m
                left join ads.ext_dim_service_writer n on m.servicewriterkey = n.servicewriterkey;
            """
            pg_cur.execute(sql)
            # bspp.sales new rows
            sql = """
                insert into bspp.sales
                select writer_id,the_date,ro,parts,labor
                from bspp.sales_tmp a
                where not exists (
                  select 1
                  from bspp.sales
                  where ro = a.ro
                    and the_date = a.the_date);
            """
            pg_cur.execute(sql)
            # bspp.sales update changed rows
            sql = """
                update bspp.sales z
                set writer_id = x.writer_id,
                    parts = x.parts,
                    labor = x.labor
                from (
                  select a.*
                  from bspp.sales_tmp a
                  inner join bspp.sales b on a.ro = b.ro
                    and a.the_date = b.the_Date
                    and (
                      a.writer_id <> b.writer_id
                      or
                      a.parts <> b.parts
                      or
                      a.labor <> b.labor)) x
                where z.ro = x.ro
                  and x.the_date = x.the_date;
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
